# Minecraft BrainF*ck Interpreter
This is a datapack taking advantage of the new macro feature in 23w31a to perform arbitrary array indexing to make a brainf*ck interpreter.

## Use
You can write a program in the first page of a book then run it via
```
/function bf:run/book {input: "your input string here"}
```
The input tag of the compound being the buffer that the comma command reads from

You can also setup the interpreter manually
```
/data modify storage bf:interpreter source set value "your source string here"
/data modify storage bf:interpreter input set value "your input string here"
/function bf:interpreter/interpret
```

## Notes
- When the input string is exhausted a null character or zero will be read.
- There are 256 cells by default, but that should be enough for any program you would want to run with this.
- The cell values and tape pointer wrap around.
- For a simple "Hello, world!" program about 28k commands are ran, I tried to optimise it as much as possible but this is the first time I've written a datapack in a while so there's probably ways it could be further optimised. However it is optimal in space compared to interpreters that have come before it which is nice.