# Write numeric type to string, trimming off type specifier
$data modify storage bf:stringify target set value "$(target)"
$execute store result storage bf:stringify value long 1 run scoreboard players get $(target) bf_math
function bf:stringify/resolved with storage bf:stringify
$data modify storage bf:interpreter $(target) set string storage bf:interpreter $(target) 0 -1