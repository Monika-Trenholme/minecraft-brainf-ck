# If char at index is equal return
$execute store success score test bf_math run data modify storage bf:interpreter char set from storage bf:char characters[$(i)]
execute if score test bf_math matches 0 run return 0

# Recopy char
data modify storage bf:interpreter char set from storage bf:op char

# Inc index
scoreboard players add i bf_math 1
function bf:stringify/main {target: "i"}

# Loop
function bf:char/read2 with storage bf:interpreter