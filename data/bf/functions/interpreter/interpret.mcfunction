# Get length of source
execute store result score source_length bf_math run data get storage bf:interpreter source

# Get length of input
execute store result score input_length bf_math run data get storage bf:interpreter input

# Initialise loop stack as an empty array
data modify storage bf:interpreter loop_stack set value []

# Validate that brackets are balanced, and create bracket jump table
scoreboard players set ip bf_math 0
data modify storage bf:interpreter ip set value "0"
scoreboard players set nip bf_math 1
data modify storage bf:interpreter nip set value "1"
scoreboard players set brackets bf_math 0
data modify storage bf:interpreter jumps set value []
function bf:interpreter/brackets with storage bf:interpreter
execute if score brackets bf_math > zero bf_math run tellraw @s {"text": "Error: unmatched opening bracket [", "color": "red"}
execute if score brackets bf_math < zero bf_math run tellraw @s {"text": "Error: unmatched closing bracket ]", "color": "red"}
execute unless score brackets bf_math = zero bf_math run return 1

# Initialise instruction pointer
scoreboard players set ip bf_math 0
data modify storage bf:interpreter ip set value "0"

# Initialise pointer to next instruction
scoreboard players set nip bf_math 1
data modify storage bf:interpreter nip set value "1"

# Initialise tape pointer
scoreboard players set tp bf_math 0
data modify storage bf:interpreter tp set value "0"

# Initialise read pointer
scoreboard players set rp bf_math 0
data modify storage bf:interpreter rp set value "0"

# Initialise pointer to next read
scoreboard players set nrp bf_math 1
data modify storage bf:interpreter nrp set value "1"

# Initialise tape with 256 cells
data modify storage bf:interpreter tape set value []
scoreboard players set i bf_math 256
function bf:interpreter/init_tape

# Initialise output string
data modify storage bf:interpreter output set value ""

# Run program and tellraw output
function bf:interpreter/main_loop with storage bf:interpreter
tellraw @s {"storage": "bf:interpreter", "nbt": "output"}