# Read current char
$data modify storage bf:interpreter current_char set string storage bf:interpreter source $(ip) $(nip)

# Find instruction
execute if data storage bf:interpreter {current_char: "+"} run function bf:op/inc_cell with storage bf:interpreter
execute if data storage bf:interpreter {current_char: "-"} run function bf:op/dec_cell with storage bf:interpreter
execute if data storage bf:interpreter {current_char: ">"} run function bf:op/inc_tp
execute if data storage bf:interpreter {current_char: "<"} run function bf:op/dec_tp
execute if data storage bf:interpreter {current_char: "["} run function bf:op/start_loop with storage bf:interpreter
execute if data storage bf:interpreter {current_char: "]"} run function bf:op/end_loop with storage bf:interpreter
execute if data storage bf:interpreter {current_char: "."} run function bf:op/write_char with storage bf:interpreter
execute if data storage bf:interpreter {current_char: ","} run function bf:op/read_char with storage bf:interpreter

# Inc ip
scoreboard players add ip bf_math 1
function bf:stringify/main {target: "ip"}

# Inc nip
scoreboard players add nip bf_math 1
function bf:stringify/main {target: "nip"}

# Loop
execute unless score ip bf_math = source_length bf_math run function bf:interpreter/main_loop with storage bf:interpreter
