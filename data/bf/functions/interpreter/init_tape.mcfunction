# Append 0s
data modify storage bf:interpreter tape append value 0s

# Dec i
scoreboard players remove i bf_math 1

# Loop
execute unless score i bf_math matches 0 run function bf:interpreter/init_tape