# Read current char
$data modify storage bf:interpreter current_char set string storage bf:interpreter source $(ip) $(nip)

# Add jump table entry for instruction
data modify storage bf:interpreter jumps append value 0l

# Match brackets
execute if data storage bf:interpreter {current_char: "["} run scoreboard players add brackets bf_math 1
execute if data storage bf:interpreter {current_char: "["} run data modify storage bf:interpreter loop_stack append value 0l
execute if data storage bf:interpreter {current_char: "["} store result storage bf:interpreter loop_stack[-1] long 1 run scoreboard players get ip bf_math
execute if data storage bf:interpreter {current_char: "]"} run scoreboard players remove brackets bf_math 1
execute if data storage bf:interpreter {current_char: "]"} store result score jump_ip bf_math run data get storage bf:interpreter loop_stack[-1]
execute if data storage bf:interpreter {current_char: "]"} run function bf:stringify/main {target: "jump_ip"}
execute if data storage bf:interpreter {current_char: "]"} run data remove storage bf:interpreter loop_stack[-1]
execute if data storage bf:interpreter {current_char: "]"} run function bf:interpreter/match_brackets with storage bf:interpreter

# Inc ip
scoreboard players add ip bf_math 1
function bf:stringify/main {target: "ip"}

# Inc nip
scoreboard players add nip bf_math 1
function bf:stringify/main {target: "nip"}

# Loop
execute unless score ip bf_math = source_length bf_math run function bf:interpreter/brackets with storage bf:interpreter