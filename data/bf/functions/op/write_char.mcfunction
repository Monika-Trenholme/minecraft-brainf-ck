# Load cell value
$execute store result score cell bf_math run data get storage bf:interpreter tape[$(tp)]
function bf:stringify/main {target: "cell"}

# Append char to output
function bf:char/write with storage bf:interpreter