# If cell value is 0 pop stack, and return
$execute store result score cell bf_math run data get storage bf:interpreter tape[$(tp)]
execute if score cell bf_math matches 0 run data remove storage bf:interpreter loop_stack[-1]
execute if score cell bf_math matches 0 run return 0

# Load ip from stack
execute store result score ip bf_math run data get storage bf:interpreter loop_stack[-1]
scoreboard players operation nip bf_math = ip bf_math
scoreboard players add nip bf_math 1