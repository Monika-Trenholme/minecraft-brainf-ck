# If reached end of input return a 0
$execute if score rp bf_math = input_length bf_math run data modify storage bf:interpreter tape[$(tp)] set value 0s
execute if score rp bf_math = input_length bf_math run return 0

# Get character index
$data modify storage bf:interpreter char set string storage bf:interpreter input $(rp) $(nrp)
function bf:char/read with storage bf:interpreter

# Store character index
$execute store result storage bf:interpreter tape[$(tp)] short 1 run scoreboard players get i bf_math

# Advance read pointer
scoreboard players add rp bf_math 1
function bf:stringify/main {target: "rp"}
scoreboard players add nrp bf_math 1
function bf:stringify/main {target: "nrp"}