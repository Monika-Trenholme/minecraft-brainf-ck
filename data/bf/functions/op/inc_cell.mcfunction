$execute store result score cell bf_math run data get storage bf:interpreter tape[$(tp)]
scoreboard players add cell bf_math 1
execute if score cell bf_math matches 256 run scoreboard players set cell bf_math 0
$execute store result storage bf:interpreter tape[$(tp)] short 1 run scoreboard players get cell bf_math