# Load cell
$execute store result score cell bf_math run data get storage bf:interpreter tape[$(tp)]

# If cell is zero jump to matching bracket and return
$execute if score cell bf_math matches 0 store result score ip bf_math run data get storage bf:interpreter jumps[$(ip)]
execute if score cell bf_math matches 0 run scoreboard players operation nip bf_math = ip bf_math
execute if score cell bf_math matches 0 run scoreboard players add nip bf_math 1
execute if score cell bf_math matches 0 run return 0

# Push ip onto loop stack
execute store result storage bf:op ip long 1 run scoreboard players get ip bf_math
data modify storage bf:interpreter loop_stack append from storage bf:op ip