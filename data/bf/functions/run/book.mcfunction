# Load source from book, and load input
data modify storage bf:interpreter source set from entity @s SelectedItem.tag.pages[0]
$data modify storage bf:interpreter input set value "$(input)"

# Run interpreter
function bf:interpreter/interpret
